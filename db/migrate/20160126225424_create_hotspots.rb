class CreateHotspots < ActiveRecord::Migration
  def change
    create_table :hotspots do |t|
      t.integer :offset_x
      t.integer :offset_y
      t.integer :radius
      t.belongs_to :product_part

      t.timestamps null: false
    end
  end
end

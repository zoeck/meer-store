class FixProductPerspectiveNameColumn < ActiveRecord::Migration
  def change
    rename_column :product_perspectives, :name, :position
  end
end

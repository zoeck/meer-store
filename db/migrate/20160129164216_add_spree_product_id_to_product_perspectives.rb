class AddSpreeProductIdToProductPerspectives < ActiveRecord::Migration
  def change
    add_reference :product_perspectives, :spree_product, index: true
  end
end
class AddProductPerspectiveIdToHotspots < ActiveRecord::Migration
  def change
    add_reference :hotspots, :product_perspective, index: true
  end
end
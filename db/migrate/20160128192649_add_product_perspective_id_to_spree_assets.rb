class AddProductPerspectiveIdToSpreeAssets < ActiveRecord::Migration
  def change
    add_reference :spree_assets, :product_perspective, index: true
  end
end

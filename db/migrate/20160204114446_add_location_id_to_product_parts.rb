class AddLocationIdToProductParts < ActiveRecord::Migration
  def change
    add_reference :product_parts, :location, index: true
  end
end
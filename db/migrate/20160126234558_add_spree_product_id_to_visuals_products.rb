class AddSpreeProductIdToVisualsProducts < ActiveRecord::Migration
  def change
    add_reference :products, :spree_product, index: true
  end
end

class CreateProductPerspectives < ActiveRecord::Migration
  def change
    create_table :product_perspectives do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end


rails g migration add_product_perspective_id_to_hotspots
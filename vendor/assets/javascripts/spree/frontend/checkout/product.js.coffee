$ ->

  Spree.addImageHandlers = ->
    thumbnails = ($ '#product-images ul.thumbnails')
    hotspotGroups = ($ '#main-image g')
    toggleClass = ($ '.selected').attr('id')

    ($ '#main-image').data 'selectedThumb', ($ '#main-image img').attr('src')

    thumbnails.find('li').eq(0).addClass 'selected'
    hotspotGroups.hide()
    hotspotGroups.eq(0).show()

    thumbnails.find('a').on 'click', (event) ->

      ($ '#main-image').data 'selectedThumb', ($ event.currentTarget).attr('href')
      ($ '#main-image').data 'selectedThumbId', ($ event.currentTarget).parent().attr('id')

      currentHotspotId = ($ event.currentTarget).parent().attr('id')
      hotspotGroups.hide()

      if not ($ '#main-image' ).hasClass('toggle')
        if not ($ event.currentTarget).parent().hasClass('off')
          ( $ '#hotspot' + currentHotspotId).show()

    thumbnails.find('li').on 'mouseenter', (event) ->
      ($ '#main-image image').attr 'xlink:href', ($ event.currentTarget).find('a').attr('href')
      currentHotspotId = ($ event.currentTarget).attr('id')
      hotspotGroups.hide()

      if not ($ '#main-image' ).hasClass('toggle')
        if not ($ event.currentTarget).hasClass('off')
          ( $ '#hotspot' + currentHotspotId).show()

    thumbnails.find('li').on 'mouseleave', (event) ->
      ($ '#main-image image').attr 'xlink:href', ($ '#main-image').data('selectedThumb')
      hotspotGroups.hide()
      currentHotspotSelected = ($ 'li.selected').attr('id')

      console.log(currentHotspotSelected)

      if not ($ '#main-image' ).hasClass('toggle')
        if not ($ event.currentTarget).hasClass('off')
          ( $ '#hotspot' + currentHotspotSelected).show()

  Spree.showVariantImages = (variantId) ->
    ($ 'li.vtmb').hide()
    ($ 'li.tmb-' + variantId).show()
    currentThumb = ($ '#' + ($ '#main-image').data('selectedThumbId'))
    if not currentThumb.hasClass('vtmb-' + variantId)
      thumb = ($ ($ '#product-images ul.thumbnails li:visible.vtmb').eq(0))
      thumb = ($ ($ '#product-images ul.thumbnails li:visible').eq(0)) unless thumb.length > 0
      newImg = thumb.find('a').attr('href')
      ($ '#product-images ul.thumbnails li').removeClass 'selected'
      thumb.addClass 'selected'
      ($ '#main-image image').attr 'xlink:href', newImg
      ($ '#main-image').data 'selectedThumb', newImg
      ($ '#main-image').data 'selectedThumbId', thumb.attr('id')

  Spree.updateVariantPrice = (variant) ->
    variantPrice = variant.data('price')
    ($ '.price.selling').text(variantPrice) if variantPrice
  radios = ($ '#product-variants input[type="radio"]')

  if radios.length > 0
    selectedRadio = ($ '#product-variants input[type="radio"][checked="checked"]')
    Spree.showVariantImages selectedRadio.attr('value')
    Spree.updateVariantPrice selectedRadio

  Spree.addImageHandlers()

  radios.click (event) ->

    hotspotGroups = ($ '#main-image g')
    thumbA = ($ '#product-images ul.thumbnails li')
    toggleId = ($ '.selected').attr('id')


    Spree.showVariantImages @value
    Spree.updateVariantPrice ($ this)


    if ($ '#main-image' ).hasClass('toggle')

      hotspotGroups.hide()

      console.log("OFF")

    unless ($ '#main-image' ).hasClass('toggle')

      hotspotGroups.hide()

      ($ '#hotspot' + toggleId).toggle()

      console.log("ON")



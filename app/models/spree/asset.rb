module Spree
  class Asset < Spree::Base
    belongs_to :viewable, polymorphic: true, touch: true
    belongs_to :visuals_product_perspective, :class_name => 'Visuals::ProductPerspective'

    acts_as_list scope: [:viewable_id, :viewable_type]
  end
end

module Visuals
  class ProductPerspective < ActiveRecord::Base
    has_one :spree_assets, :class_name => 'Spree::Asset', :foreign_key => :product_perspective_id
    has_many :hotspots, :class_name => 'Visuals::Hotspot', :foreign_key => :product_perspective_id
    has_one :spree_image, :class_name => 'Spree::Image'

    belongs_to :spree_product, :class_name => 'Spree::Product', :foreign_key => :spree_product_id

  end
end

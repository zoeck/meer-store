module Visuals
  class Part < ActiveRecord::Base
  has_many :product_parts
  has_many :products, through: :product_parts

  end
end

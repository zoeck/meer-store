module Visuals
  class ProductPart < ActiveRecord::Base
  belongs_to :material
  belongs_to :product
  belongs_to :part
  has_many :hotspots
  belongs_to :location

  def part_slug
    self.part.name.downcase
  end

  def hotspot
    self.hotspots.first
  end

  end
end
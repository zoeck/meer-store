module Visuals
  class Material < ActiveRecord::Base
  has_many :product_parts

  extend FriendlyId
  friendly_id :name

  end
end


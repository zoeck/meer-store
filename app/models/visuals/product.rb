module Visuals
  class Product < ActiveRecord::Base
  has_many :product_colors
  has_many :colors, through: :product_colors

  has_many :product_parts
  has_many :parts, through: :product_parts

  belongs_to :spree_product, :class_name => 'Spree::Product', :foreign_key => :spree_product_id

  def partial_name
    self.name.downcase
  end

  extend FriendlyId
  friendly_id :name, use: :slugged

  end
end
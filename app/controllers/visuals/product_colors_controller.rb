module Visuals
  class ProductColorsController < ApplicationController
  def index
    @product_colors = ProductColor.all

  end

  def show
    @product = Product.friendly.find(params[:id])
    @product_colors = ProductColor.all
    @product_color = ProductColor.find(params[:id])
  end

  def new
    @products = Product.all
    @parts = Part.all
    @product_color = ProductColor.new
  end

  def edit
    @products = Product.all
    @product_color = ProductColor.find(params[:id])
  end

  def create
    @product_color = ProductColor.new(product_color_params)

    if @product_color.save
      redirect_to visuals_product_colors_path
    else
      render 'new'
    end
  end

  def update
    @product_color = ProductColor.find(params[:id])

    if @product_color.update(product_color_params)
      redirect_to visuals_product_colors_path
    else
      render 'edit'
    end
  end

  def destroy
    @product_part = ProductColor.find(params[:id])
    @product_part.destroy

    redirect_to visuals_product_colors_path
  end

  private
  def product_color_params
    params.require(:product_color).permit(:name, :product_id, :part_id)
  end

  end
end
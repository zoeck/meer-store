module Visuals
  class ProductsController < ApplicationController
   def index
    @products = Product.all
  end

  def show
    @products = Product.all
    @product = Product.friendly.find(params[:id])
    @product_parts = @product.product_parts
    @product_colors = @product.product_colors

  end

def new
  @product = Product.new
end

def edit
  @product = Product.friendly.find(params[:id])
end

def create
  @product = Product.new(product_params)
 
  if @product.save
    redirect_to visual_products_path
  else
    render 'new'
  end
end

def update
  @product = Product.friendly.find(params[:id])
 
  if @product.update(product_params)
    redirect_to visual_products_path
  else
    render 'edit'
  end
end

def destroy
  @product = Product.friendly.find(params[:id])
  @product.destroy
 
  redirect_to visual_products_path
end
private
  def product_params
    params.require(:product).permit(:name)
  end

end
end
module Visuals
  class MaterialsController < ApplicationController
  def index
    @materials = Material.all
  end

  def show
    @materials = Material.all
    @material = Material.friendly.find(params[:id])
  end

  def new
    @material = Material.new
  end

  def edit
    @material = Material.friendly.find(params[:id])
  end

  def create
    @material = Material.new(material_params)

    if @material.save
      redirect_to visuals_materials_path
    else
      render 'new'
    end
  end

  def update
    @material = Material.friendly.find(params[:id])

    if @material.update(material_params)
      redirect_to visual_materials_path
    else
      render 'edit'
    end
  end

  def destroy
    @material = Material.friendly.find(params[:id])
    @material.destroy

    redirect_to visuals_materials_path
  end

  private
  def material_params
    params.require(:material).permit(:name)
  end

  end
  end
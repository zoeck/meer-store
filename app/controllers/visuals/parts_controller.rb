module Visuals
class PartsController < Visuals::VisualsController
  layout "parts"

   def index
    @parts = Part.all
  end

  def show
    @parts = Part.all
    @part = Part.find(params[:id])
  end

def new
  @part = Part.new
  @products = Product.all
  @materials = Material.all
end

def edit
  @part = Part.find(params[:id])
end

def create
  @part = Part.new(part_params)
 
  if @part.save
    redirect_to visuals_parts_path
  else
    render 'new'
  end
end

def update
  @part = Part.find(params[:id])
 
  if @part.update(part_params)
    redirect_to visuals_parts_path
  else
    render 'edit'
  end
end

def destroy
  @part = Part.find(params[:id])
  @part.destroy
 
  redirect_to visuals_parts_path
end

private
  def part_params
    params.permit(:name, :description)
  end

end
end
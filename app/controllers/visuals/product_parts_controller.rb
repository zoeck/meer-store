module Visuals
  class ProductPartsController < ApplicationController
   def index
    @product_parts = ProductPart.all

  end

  def show
    @product_parts = ProductPart.all
    @product_part = ProductPart.friendly.find(params[:id])
  end

def new
  @products = Product.all
  @parts = Part.all
  @product_part = ProductPart.new
end

def edit
  @product_part = ProductPart.friendly.find(params[:id])
end

def create
  @product_part = ProductPart.new(product_part_params)

  if @product_part.save
    redirect_to visuals_product_product_parts_path
  else
    render 'new'
  end
end

def update
  @product_part = ProductPart.friendly.find(params[:id])

  if @product_part.update(product_part_params)
    redirect_to visuals_product_product_parts_path
  else
    render 'edit'
  end
end

def destroy
  @product_part = ProductPart.friendly.find(params[:id])
  @product_part.destroy

  redirect_to visuals_product_product_parts_path
end

private
  def product_part_params
    params.require(:product_part).permit(:name, :product_id, :part_id)
  end

  end
  end